<?php
/**
 * Created by PhpStorm.
 * User: naet
 * Date: 15.07.17
 * Time: 18:10
 */

namespace app\core;


class Resolver
{
    private $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function resolve()
    {
        switch (gettype($this->result)) {
            case "object" :
                if ($this->result instanceof View ){
                    $view = $this->result;
                    return $view->generate();
                } break;
            case "array" :
                return json_encode($this->result);
            case "string":
                 return $this->result;
            default: return false;
        }
    }
}