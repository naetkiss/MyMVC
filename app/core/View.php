<?php
namespace app\core;
/**
 * Created by PhpStorm.
 * User: naet
 * Date: 14.07.17
 * Time: 12:23
 */
class View
{
    /**
     * имя файла view относительно /resources/view
     *
     * @var string
     */
    private $name;

    /**
     * переданные во вью переменные, где ключ имя переменной
     *
     * @var array
     */
    private $data;

    public $param;

    /**
     * View constructor.
     * @param $name
     * @param array $data
     */
    public function __construct($name, array $data = [])
    {
        $this->name = $name;
        $this->data = $data;

    }

    /**
     * Рендерит и возвращает вью
     *
     * @return view
     */
    public function generate()
    {
        ob_start();
        extract($this->data);
        require_once ROOT.'/resources/views/'.$this->name.".php";
        return ob_get_clean();


    }
}