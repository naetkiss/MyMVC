<?php
/**
 * Created by PhpStorm.
 * User: naet
 * Date: 14.07.17
 * Time: 13:56
 */

namespace app\models;


use app\core\Model;

Class AuthModel extends Model
{
    public function all()
    {
        $db = $this->connectDB;
        $preparedQuery = $db->query("SELECT * FROM user");
        $select = $preparedQuery->fetchAll();
        return $select;
    }

}