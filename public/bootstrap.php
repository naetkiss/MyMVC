<?php
/**
 * Created by PhpStorm.
 * User: naet
 * Date: 14.07.17
 * Time: 11:14
 */

require_once ROOT . '/autoload.php';
require_once ROOT . '/Routes/routes.php';
if (\app\core\Route::get($_SERVER['REQUEST_URI'])) {
    list($controller, $method) = explode('@', \app\core\Route::get($_SERVER['REQUEST_URI']));
} else {
    throw new Exception('Страница не найдена', 404);
}

$reflectionClass = new ReflectionClass('app\\controllers\\'.$controller);
$controllerInstance = $reflectionClass->newInstance();
$resolver = new \app\core\Resolver($controllerInstance->$method());
echo $resolver->resolve();


