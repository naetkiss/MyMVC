<?php

namespace app\controllers;
use app\core\Controller;
use app\core\View;
use app\models\DBModel;

/**
 * Created by PhpStorm.
 * User: naet
 * Date: 14.07.17
 * Time: 13:52
 */
class AddTaskController extends Controller
{

    public function action( )
    {
        $catalog = ROOT."/public/file/".time()."_".$_FILES['pick']['name'];
        $file = copy($_FILES['pick']['tmp_name'], $catalog);
        $pathFile = "file/".time()."_".$_FILES['pick']['name'];
        $q = new DBModel();
        $result = $q->newTask($_REQUEST['task'], $_REQUEST['username'], $_REQUEST['e_mail'], $pathFile);
        header("Location: /");
        return $result;
    }
}