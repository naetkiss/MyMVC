<?php
/**
 * Created by PhpStorm.
 * User: naet
 * Date: 15.07.17
 * Time: 22:12
 */

namespace app\models;

//use app\core\Model;

use app\core\Model;

class DBModel extends Model
{
    public function all()
    {
        $db = $this->connectDB;
        $preparedQuery = $db->query("SELECT * FROM task");
        $select = $preparedQuery->fetchAll();
        return $select;
    }

    public function count()
    {
        $db = $this->connectDB;
        $preparedQuery = $db->query("SELECT COUNT(*) as count FROM task");
        $select = $preparedQuery->fetch();
        return $select['count'];
    }

    public function selectRecord    ($numberPage, $recordPerPage)
    {
        $db = $this->connectDB;
        $offset = $numberPage*$recordPerPage-$recordPerPage;
        $preparedQuery = $db->query("SELECT * FROM task ORDER BY id LIMIT $recordPerPage OFFSET $offset");
        $select[] = $preparedQuery->fetchAll();
        return $select;

    }

    public function newTask($task, $username, $e_mail, $pick)
    {
        $db = $this->connectDB;
        $sql = 'INSERT INTO task (task, username, e_mail, file)
        VALUES (:task,:username, :e_mail, :file)';
        $sth = $db->prepare($sql);
        return $sth->execute(array(':task' => $task, ':username' => $username, ':e_mail' => $e_mail, ':file' => $pick ));
    }
}