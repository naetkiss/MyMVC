<?php
namespace app\core;

/**
 * Created by PhpStorm.
 * User: naet
 * Date: 14.07.17
 * Time: 13:05
 */
class Model
{
    private $dsn;
    private $user;
    private $pass;
    protected $connectDB;

    public function __construct()
    {
        $this->dsn = DB_DSN;
        $this->user = DB_USER;
        $this->pass = DB_PASS;
        $this->connectDB = new \PDO($this->dsn, $this->user,$this->pass);

    }

    public function getConnectionDB()
    {
        return $this->connectDB;
    }
}
