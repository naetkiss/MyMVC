<?php

namespace app\controllers;
use app\core\Controller;
use app\core\Model;
use app\core\View;
use app\models\DBModel;
use app\models\TaskModel;

/**
 * Created by PhpStorm.
 * User: naet
 * Date: 14.07.17
 * Time: 13:52
 */
class taskController extends Controller
{

    public function action()
    {

        $db = new DBModel();
        $recordPerPage = 3;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;

        $lastPage = ceil(($db->count())/$recordPerPage) ;
        $prevPage = ($page != 1) ? $page-1 : null;
        $nextPage = ($page != $lastPage) ? $page+1 : null;
        $allTask = $db->selectRecord($page,$recordPerPage);
        return new View("newtask", ['allTask' => $allTask, 'page' => $page,'nextpage' => $nextPage, 'prevpage' => $prevPage,'lastpage' => $lastPage]);
    }
}