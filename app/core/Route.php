<?php

namespace app\core;

class Route
{
    private static $maps = [];
    private $userUrl;


    public static function register($url,$handler)
    {
        if($url != '/') {
            $url = trim($url, '/');
        }

        self::$maps[$url] = $handler;
    }

    public static function get($userUrl)
    {
        $userUrl = strtok($userUrl, '?');
        if ($userUrl != '/') {
            $userUrl = trim($userUrl,"/");
        }

        $handler = null;

        if (isset(self::$maps[$userUrl])) {
            $handler = self::$maps[$userUrl];
        };
        return $handler;
    }
}
