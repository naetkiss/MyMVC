<?php
use app\core\Route;

Route::register("/", "taskController@action");
Route::register("/addtask", "AddTaskController@action");
Route::register("/auth", "authController@action");
Route::register("/id", "authController@auth");

