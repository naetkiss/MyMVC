<html>
<?php include 'header.php';
$formstyle = 'control-label'
?>

<body>
<a href="/auth" name="page" class="btn btn-small" >Авторзация</a>

    <div class="form-action" style="margin: 1% 0 0 2%; width: 60%">
            <form  id="newtask" enctype="multipart/form-data" method="post" action="/addtask" >

                <blockquote style="<?=$formstyle?>"> Новая задача: </<blockquote>
                <div class="control-group" >
                    <label class="<?=$formstyle?>" style="width: 140px">Имя пользователя</label>
                    <input type="text" name="username" value="" placeholder="Имя пользователя"></label>
                </div>
                <div class="control-group">
                    <label class="<?=$formstyle?>" style="width: 140px">Задача</label>
                    <input type="text" name="task" placeholder="Текст задачи">
                </div>
                <div class="control-group">
                    <label class="<?=$formstyle?>" style="width: 140px">e-mail</label>
                    <input type="email" name="e_mail" placeholder="e-mail"><br>
                </div>
                <input type="file" name="pick" value="Прикрепить картинку" class="btn btn-large btn-block" style="width:35%">
                <input type="submit" name="send" value="Добавить задачу"  class="btn btn-large btn-block btn-primary" style=" width: 35%; margin-left: 0.5%">
            </form>
    </div>
    <table class="table table-bordered" style="width: 70%; margin-left: 2%">
        <th>
            <td>Задача</td>
            <td>Автор</td>
            <td>Статус</td>
        </th>
        <?php

     foreach ($allTask as $task ): ?>

         <?php foreach ($task as $key ):?>
           <tr >
           <td><?=$key["id"];?></td>
           <td style="width: 50%"><?=$key["task"];?></td>
           <td><?=$key["username"];?></td>
           <?php if ($key["done"]==1):?>
               <td style="width: 1%; height: 1%"><img style="width: auto; height: 10%" > <img src="/file/done1.png" style="width: 20px; height: 20px"></td>
           <?php else:?>
               <td style="width: 1%; height: 1%"><img style="width: auto; height: 10%"  ></td>
           <?php endif;?>
           </tr>
       <?php endforeach; ?>
   <?php endforeach;
   ?>

    </table>
    <div style="width: 70%; height: auto; margin-left: 5%">
    <?php if ($prevpage != null):?>
    <a href="/?page=<?=1 ?>" name="page" class="btn btn-small">Первая страница</a>
    <a href="/?page=<?=$prevpage ?>" name="page" class="btn btn-small" >Назад</a>
    <?php endif; ?>
    <span  name="page" class="btn btn-small"><?=$page?></span>
    <?php if ($nextpage != null):?>
    <a href="/?page=<?=$nextpage ?>" name="page" class="btn btn-small" >Вперёд</a>
    <a href="/?page=<?=$lastpage ?>" name="page" class="btn btn-small">Последняя страница</a>
    <?php endif;
    ?>
    </div>
</body>
</html>